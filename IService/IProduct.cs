﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestGraphQL_1.Model;

namespace TestGraphQL_1.IService
{
   public interface IProduct
    {
        public List<Product> GetProducts();
    }
}
