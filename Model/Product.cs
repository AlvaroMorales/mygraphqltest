﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestGraphQL_1.Model
{
    public class Product
    {
        public int productId { get; set; }
        public string name { get; set; }
        public int category { get; set; }
    }
}
