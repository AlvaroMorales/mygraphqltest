﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestGraphQL_1.IService;
using TestGraphQL_1.Service;

namespace TestGraphQL_1.Model
{
    public class Query
    {
        IProduct _productService;
        public Query(IProduct productService)
        {
            this._productService = productService;
        }
        public List<Product> Products => _productService.GetProducts();

    }
}
