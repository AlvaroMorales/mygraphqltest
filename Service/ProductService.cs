﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestGraphQL_1.IService;
using TestGraphQL_1.Model;

namespace TestGraphQL_1.Service
{
    public class ProductService: IProduct
    {
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            for (int i = 0; i < 10; i++)
            {
                products.Add(new Product
                {
                    name = $"test {i}",
                    productId = i,
                    category = i > 5 ? 1 : 2
                });
            }
            return products;
        }
        
    }
}
